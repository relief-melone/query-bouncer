import 'express';
import { Router } from 'express';
import sinon, { SinonStub } from 'sinon';

interface ExecOpts {
  method: 'GET' | 'POST' | 'PUT' | 'DELETE',
  url: string,
}

export default class RouteTester {
  router: Router;
  req:any;
  res: {
    status: SinonStub,
    json: SinonStub,
    send: SinonStub,
    end: SinonStub
  };
  hasEnded: Promise<boolean>;

  constructor(router:Router, req?:any){
    let resolveHasEnded;
    
    this.req = req || {};
    this.req.params = req?.params || {};
    this.req.query = req?.query || {};
    this.req.body = req?.body || {};
    this.req.headers = req?.headers || {};
    

    this.hasEnded = new Promise((res) => {
      resolveHasEnded = res;
    });
    
    this.res = {
      status: sinon.stub(),
      json: sinon.stub(),
      send: sinon.stub(),
      end: sinon.stub()
    };

    this.res.status.returns(this.res);    
    this.res.json.returns(this.res);    
    this.res.send.returns(this.res);
    this.res.end.returns(this.res);

    this.res.json.callsFake(() => resolveHasEnded(true));
    this.res.send.callsFake(() => resolveHasEnded(true));
    this.res.end.callsFake(() => resolveHasEnded(true));

    this.router = router;
  }

  async execute(opts:ExecOpts):Promise<void>{
    (this.router as any).handle(
      Object.assign({}, this.req, { method: opts.method, url: opts.url }),
      this.res,
    );
    await this.hasEnded;
  }
}