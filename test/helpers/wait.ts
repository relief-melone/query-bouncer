export default (timeoutMd=50, reject=false):Promise<void> => new Promise((res, rej) => {
  setTimeout(() => 
    reject 
      ? rej()
      : res()    
  , timeoutMd );
});