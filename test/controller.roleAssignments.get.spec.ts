import sinon from 'sinon';

import { getController as getRoleAsssignmentsController } from '../src/controllers/controller.roleAssignment.get';
import errorHandler from '../src/controllers/errors/controller.errorHandler';
import IRoleAssignment from '../src/interfaces/interface.RoleAssignment';

describe('controller.roleAssignments.get', () => {
  let res;
  let next;
  let getRoleAssignments;

  const roleAssignmentToGet: IRoleAssignment = {
    User: 'Some_User',
    Role: 'someRole',
    Data: {}
  };


  beforeEach(() => {
    res = sinon.stub({        
      json() { },
      status(){ },
    });
    res.json.returns(res);
    res.status.returns(res);
    next = sinon.stub();
    getRoleAssignments = sinon.stub();
  });

  it('will get roleAssignments', async () => {
    // Prepare
    const req = { };
    getRoleAssignments.returns([roleAssignmentToGet]);
    const nonLowerCasingMainConfig ={ adminToken:'123', forceUserToLowerCase:false, userPrimaryKey:'123' };
    // Execute
    await getRoleAsssignmentsController(
      errorHandler, 
      getRoleAssignments,
    )( req as any, res, next );

    // Assert
    sinon.assert.calledOnce(getRoleAssignments);
    sinon.assert.calledWith(res.status,200);
    sinon.assert.calledWith(res.json, [roleAssignmentToGet]);
  });
  
  it('will get roleAssignments in lowercase', async () => {
    // Prepare
    const req = { };
    const lowercasedRoleAssignment: IRoleAssignment = {
      User: 'some_user',
      Role: 'someRole',
      Data: {}
    };

    getRoleAssignments.returns([roleAssignmentToGet]);

    // Execute
    await getRoleAsssignmentsController(
      errorHandler, 
      getRoleAssignments
    )( req as any, res, next );

    // Assert
    sinon.assert.calledOnce(getRoleAssignments);
    sinon.assert.calledWith(res.status,200);
    sinon.assert.calledWith(res.json, [lowercasedRoleAssignment]);
  });


  it('will return an error if the desired role does not exist', async () => {
    // Prepare
    const req = {
    };
    getRoleAssignments.throws();
    
    // Execute
    await getRoleAsssignmentsController(
      errorHandler, 
      getRoleAssignments
    )( req as any, res, next );

    // Assert
    sinon.assert.calledWith(res.status, 500);
  });
});