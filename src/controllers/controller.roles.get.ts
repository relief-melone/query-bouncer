import { RequestHandler } from 'express';
import GetRoles from '../services/internal/service.roles.get';
import ErrorHandler from './errors/controller.errorHandler';

export const getController = (
  errorHandler = ErrorHandler,
  getRoles = GetRoles,
):RequestHandler => 
  async (_req, res, _next) => {
    try {
      const roles = await getRoles();
      return res.status(200).json(roles);
    } catch (err) {
      return errorHandler(err, res);
    }
  };

export default getController();