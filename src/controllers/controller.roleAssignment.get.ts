import { RequestHandler } from 'express';
import ConfigMain from '../configs/config.main';
import GetRoleAssignments from '../services/internal/service.roleAssignments.get';
import ErrorHandler from './errors/controller.errorHandler';

export const getController = (
  errorHandler= ErrorHandler,
  getRoleAssignments = GetRoleAssignments,
  configMain = ConfigMain
):RequestHandler => 
  async (_, res) => {
    try {
      const roleAssignments = await getRoleAssignments();
      if (configMain.forceUserToLowerCase){
        roleAssignments.forEach(ra=>ra.User = ra.User.toLowerCase());
      }
      return res.status(200).json(roleAssignments);
    } catch (err) {
      return errorHandler(err, res);
    }
  };

export default getController();