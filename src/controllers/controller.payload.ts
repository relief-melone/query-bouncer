import { RequestHandler } from 'express';
import MainConfig from '../configs/config.main';
import ErrorHandler from '../controllers/errors/controller.errorHandler';
import GetPermissions from '../services/queries/service.getBusinessPermissionsByTitles';
import GetRoleAssignmentsForUser from '../services/queries/service.getRoleAssignmentsForUser';
import GetRoleByTitle from '../services/queries/service.getRoleByTitle';

import { getController as getPayloadController } from './controller.payload.function';

const getController = (
  config = MainConfig,
  getRoleAssignmentsForUser = GetRoleAssignmentsForUser,
  getRoleByTitle = GetRoleByTitle,
  getPermissions = GetPermissions,
  errorHandler = ErrorHandler
):RequestHandler => 
  (req,res,next) =>  getPayloadController(config, getRoleAssignmentsForUser, getRoleByTitle, getPermissions, errorHandler)(req, res, next);

export default getController();