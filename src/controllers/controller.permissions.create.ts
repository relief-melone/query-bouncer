import { RequestHandler } from 'express';

import ErrorHandler from './errors/controller.errorHandler';

export default (createPermission, errorHandler = ErrorHandler):RequestHandler => 
  async ( req, res ) => {
    try {
      const permission = await createPermission(req.body);
      return res.status(201).json(permission);
    } catch (err) {
      return errorHandler(err, res);
    }
  };