import PayloadController from '@/controllers/controller.payload';
import PayloadAndQueryController from '@/controllers/controller.payloadAndQuery';
import QueryController from '@/controllers/controller.query';
import { RequestHandler } from 'express';
import { Right } from '../interfaces/interface.Permission';
import errorFactory from '../services/error/service.errors';
import validateRight from '../services/validation/service.validateRight';

export const getMainController = (
  queryController = QueryController,
  payloadController = PayloadController,
  payloadAndQueryController = PayloadAndQueryController
):RequestHandler => 
  (req, res, next) => {
    try {
      const right = validateRight(req.params.Right);
      if (!right)
        return res.status(400).json(errorFactory.badAttributeInput('Please provide either read, create, delete or update as Right'));

      switch (right){
        case Right.read:
        case Right.delete:
          return queryController(req,res,next);
        case Right.create:
          return payloadController(req,res,next);
        case Right.update:
          return payloadAndQueryController(req,res,next);
      }
    } catch (err) {
      res.status(500).json(errorFactory.internalServerError('Internal Server error'));
    }    
  };

export default getMainController();