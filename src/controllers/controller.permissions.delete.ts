import { RequestHandler } from 'express';
import errorFactory from '../services/error/service.errors';
import ErrorHandler from './errors/controller.errorHandler';

export default (  deletePermission ,errorHandler = ErrorHandler ):RequestHandler => 
  async ( req, res) =>  {
    try {
      const title = req.params.title;
      const permission = await deletePermission(title);
      if (!permission){
        return errorHandler(errorFactory.documentNotFound({ Document:title }), res);
      }
      return res.status(200).json(permission);
    } catch (err) {
      return errorHandler(err, res);
    }
  };