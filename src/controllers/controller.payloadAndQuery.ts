import { RequestHandler } from 'express';
import MainConfig from '../configs/config.main';
import errorFactory from '../services/error/service.errors';
import populateInputRestrictions from '../services/input/service.populateInputRestrictions';
import combinePermissionsAndQuery from '../services/queries/service.combinePermissionsAndQuery';
import populateQueryRestrictions from '../services/queries/service.populatePermissionQueries';
import validatePayloadAgainstRestrictions from '../services/queries/service.validatePayloadAgainstRestrictions';
import validateRight from '../services/validation/service.validateRight';

import ErrorHandler from '../controllers/errors/controller.errorHandler';
import GetPermissions from '../services/queries/service.getBusinessPermissionsByTitles';
import GetRoleAssignmentsForUser from '../services/queries/service.getRoleAssignmentsForUser';
import GetRoleByTitle from '../services/queries/service.getRoleByTitle';

export const getController = (
  config = MainConfig,
  getRoleAssignmentsForUser = GetRoleAssignmentsForUser,
  getRoleByTitle = GetRoleByTitle,
  getPermissions = GetPermissions,
  errorHandler = ErrorHandler
):RequestHandler => 
  async (req, res, _next) => {
    const user = req.user ? req.user[config.userPrimaryKey] : null;
    const collection = req.params.Collection;
    const right = validateRight(req.params.Right);

    if (!right)
      throw errorFactory.badAttributeInput('Please provide either read, create, delete or update as Right');
  
    const payload = req.body.payload;
    const query = req.body.query;
    
    try {
      const roleAssignments = await getRoleAssignmentsForUser(user);
      const populatedInputRestrictions: any = [];
      const populatedQueryRestrictions: any = [];

      await Promise.all(roleAssignments.map(async roleAssignment => {
        const role = await getRoleByTitle(roleAssignment.Role);
        if (role){
          const permissions = await getPermissions(right, collection, role.Permissions);
          populatedInputRestrictions.push(...populateInputRestrictions(permissions, roleAssignment.Data));
          populatedQueryRestrictions.push(...populateQueryRestrictions(permissions, roleAssignment.Data));
        }
        return;
      }));
    
      validatePayloadAgainstRestrictions(payload, populatedInputRestrictions);
      if (populatedQueryRestrictions.length === 0){
        return errorHandler(errorFactory.unauthorized('No Permission was found'), res);
      }

      return res.status(200).json({
        query: combinePermissionsAndQuery(query, populatedQueryRestrictions),
        payload
      });
    
    } catch (err) {
      return errorHandler(err, res);
    }
  };



export default getController();