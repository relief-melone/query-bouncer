import { RequestHandler } from 'express';
import DeleteRole from '../services/internal/service.roles.delete';
import ErrorHandler from './errors/controller.errorHandler';

export const getController = (
  errorHandler = ErrorHandler,
  deleteRole = DeleteRole
):RequestHandler => 
  async (req,res) => {
    try {
      const role = await deleteRole(req.params.title);
      return res.status(200).json(role);
    } catch (err) {
      return errorHandler(err, res);
    }
  };

export default getController();