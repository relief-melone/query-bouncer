import configMain from '@/configs/config.main';
import GetPermissions from '@/services/internal/service.permissions.getBy';

import GetRoleAssignmentsForUser from '@/services/queries/service.getRoleAssignmentsForUser';
import GetRoleByTitle from '@/services/queries/service.getRoleByTitle';
import { RequestHandler } from 'express';
import populateInputRestrictions from '../services/input/service.populateInputRestrictions';
import validatePayloadAgainstRestrictions from '../services/queries/service.validatePayloadAgainstRestrictions';
import validateRight from '../services/validation/service.validateRight';
import ErrorHandler from './errors/controller.errorHandler';

import errorFactory from '@/services/error/service.errors';


export const getController = (
  config = configMain, 
  getRoleAssignmentsForUser = GetRoleAssignmentsForUser, 
  getRoleByTitle = GetRoleByTitle, 
  getPermissions = GetPermissions, 
  errorHandler = ErrorHandler
):RequestHandler =>
  async (req,res, _next) => {
    const user =  req.user ? req.user[config.userPrimaryKey] : null;
    const collection = req.params.Collection;
    const right = validateRight(req.params.Right);

    if (!right)
      throw errorFactory.badAttributeInput('Please provide either read, create, delete or update as Right');

    const payload = req.body.payload;
      
    try {
      const roleAssignments = await getRoleAssignmentsForUser(user);
  
      const populatedPermissions: any[] = (await Promise.all(roleAssignments.map(async roleAssignment => {
        const role = await getRoleByTitle(roleAssignment.Role);
        if (role){
          const permissions = await getPermissions(right, collection, role.Permissions);
          return await populateInputRestrictions(permissions, roleAssignment.Data);
        } else {
          return [];
        }
      }))).flat();
      
      validatePayloadAgainstRestrictions(payload, populatedPermissions);
      return res.status(200).json({
        payload
      });
      
    } catch (err) {
      return errorHandler(err, res);
    }    
  }; 


export default getController();