import { RequestHandler } from 'express';
import Permission from '../interfaces/interface.Permission';
import ErrorHandler from './errors/controller.errorHandler';

export default (  updatePermission,errorHandler= ErrorHandler):RequestHandler => 
  async ( req, res ) => {
    try {
      const permit = req.body as Permission;
      const title = req.params.title;
      const permission = await updatePermission(title, permit);
      return res.status(200).json(permission);
    } catch (err) {
      return errorHandler(err, res);
    }
  };