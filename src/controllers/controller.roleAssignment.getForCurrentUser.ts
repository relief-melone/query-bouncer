import { RequestHandler } from 'express';
import ConfigMain from '../configs/config.main';
import GetRoleAssignmentsForUser from '../services/queries/service.getRoleAssignmentsForUser';
import ErrorHandler from './errors/controller.errorHandler';

export const getController = (
  errorHandler= ErrorHandler,
  getRoleAssignmentForUser = GetRoleAssignmentsForUser,
  configMain = ConfigMain
):RequestHandler => 
  async (req,res,_) => {
    try {
      const user = (req as any).user[configMain.userPrimaryKey];
      const roleAssignments = await getRoleAssignmentForUser(user);
      if (configMain.forceUserToLowerCase){
        roleAssignments.forEach(ra=>ra.User = ra.User.toLowerCase());
      }
      return res.status(200).json(roleAssignments);
    } catch (err) {
      return errorHandler(err, res);
    }
  };

export default getController();