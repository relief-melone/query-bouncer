import { RequestHandler } from 'express';
import { Model } from 'mongoose';
import configMain from '../configs/config.main';
import ErrorHandler from './errors/controller.errorHandler';

import Permission from '@/interfaces/interface.Permission';
import { getService as getServicePopulatedPermissionsForUser } from '../services/queries/service.getPermissionsForUser';

export default (  
  model: Model<Permission>,
  errorHandler = ErrorHandler
):RequestHandler =>
  async ( req, res ) => {
    const user = (req as any).user;
    const username = user? user[configMain.userPrimaryKey] :null;
    try {
      const permissions = await getServicePopulatedPermissionsForUser(model)(username);
      return res.status(200).json(permissions);
    } catch (err) {
      return errorHandler(err, res);
    }
  };