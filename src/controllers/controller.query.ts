import { RequestHandler, Response } from 'express';
import errorFactory from '../services/error/service.errors';
import combinePermissionsAndQuery from '../services/queries/service.combinePermissionsAndQuery';
import populatePermissionQueries from '../services/queries/service.populatePermissionQueries';
import validateRight from '../services/validation/service.validateRight';

import configMain from '@/configs/config.main';
import log from '@/log/logger.default';
import GetPermissions from '@/services/internal/service.permissions.getBy';
import GetRoleAssignmentsForUser from '@/services/queries/service.getRoleAssignmentsForUser';
import GetRoleByTitle from '@/services/queries/service.getRoleByTitle';


export const getQueryController = (
  config = configMain, 
  getRoleAssignmentsForUser = GetRoleAssignmentsForUser,
  getRoleByTitle = GetRoleByTitle,
  getPermissions = GetPermissions,
):RequestHandler => 
  async ( req, res, _): Promise<Response> => {
    
    const user: string | null = (req as any).user ? (req as any).user[config.userPrimaryKey] : null;
    const collection = req.params.Collection;
    const right = validateRight(req.params.Right);

    if (!right)
      return res.status(400).json(errorFactory.badAttributeInput('Please provide either read, create, delete or update as Right'));

    log.info(`Incoming ${right} request for:   user: ${user}   collection: ${collection}`);
    if (!req.body.query)
    // Remove option to pass request without query with upcoming major relase
      console.log('Sending query directly in body is deprecated. Please nest in query Object');
  
    const query = req.body.query ? req.body.query : req.body;

    const roleAssignments = await getRoleAssignmentsForUser(user);

    const populatedPermissions: any[] = (await Promise.all(roleAssignments.map(async roleAssignment => {
      const role = await getRoleByTitle(roleAssignment.Role);
      if (role){
        const permissions = await getPermissions(right, collection, role.Permissions);
        return populatePermissionQueries(permissions, roleAssignment.Data);
      } else {
        return [];
      }
    }))).flat();
    
    if (populatedPermissions.length === 0){
      log.info(`No ${right} permission found for user ${user} and ${collection}`);
      return res.status(403).json(errorFactory.unauthorized('No Permission was found'));
    } 

    const result = {
      query: combinePermissionsAndQuery(query, populatedPermissions)
    };
    log.info('controller.query: Returning result...');
    log.info(result);
    return res.status(200).json(result);
  };

export default getQueryController();