import Permission, { Right } from '../../interfaces/interface.Permission';
import PermissionModel from '../../models/model.permission';

export const getService =  (permissionModel = PermissionModel )=> 
  async (operationType: Right, collection: string, Titles: string[]): Promise<Array<Permission>> => {
    const query = {
      Right: Right[operationType],
      Collection: collection,
      Title: { $in: Titles }
    };

    const permissions = await permissionModel.find(query);
    return permissions.map(pm => pm.toObject());
  };

export default getService();