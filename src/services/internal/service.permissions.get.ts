import IPermission from '../../interfaces/interface.Permission';
import PermissionModel from '../../models/model.permission';

export default (permissionModel: typeof PermissionModel) => async (): Promise<IPermission[]> =>{
  const permissions = await permissionModel.find();
  return permissions.map(p=>p.toObject());
};