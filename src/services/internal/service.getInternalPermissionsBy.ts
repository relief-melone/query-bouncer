import InternalPermissionModel from '../../models/model.internalPermission';
import { getService as getPermissionsBy } from './service.permissions.getBy';


export default getPermissionsBy(InternalPermissionModel);
