import IPermission from '../../interfaces/interface.Permission';
import PermissionModel from '../../models/model.permission';

export default (permissionsModel: typeof PermissionModel )=> async (Permission: IPermission): Promise<IPermission> => {
  const permission = await permissionsModel.create(Permission);
  return permission.toObject();
};
