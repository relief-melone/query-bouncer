import IRole from '../../interfaces/interface.Role';
import RoleModel from '../../models/model.role';

export default async (Title: string,Role: IRole): Promise<IRole | null> => {
  const role = await RoleModel.findOneAndUpdate({ Title },Role,{ new: true });
  return role ? role.toObject() : null;
};