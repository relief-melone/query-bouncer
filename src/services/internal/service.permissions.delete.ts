import IPermission from '../../interfaces/interface.Permission';
import PermissionModel from '../../models/model.permission';

export default (permissionModel: typeof PermissionModel) => async (Title: string): Promise<IPermission | null> =>{
  const permission = await permissionModel.findOneAndDelete({ Title });
  return permission ? permission.toObject() : null;
};