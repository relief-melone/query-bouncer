import IRoleAssignment from '../../interfaces/interface.RoleAssignment';
import RoleAssignmentModel from '../../models/model.roleAssignment';

export default async ( id: string ): Promise<IRoleAssignment | null> => {
  const deletedRoleAssignment = await RoleAssignmentModel.findOneAndDelete({ _id:id });
  return deletedRoleAssignment? deletedRoleAssignment.toObject(): null;
};