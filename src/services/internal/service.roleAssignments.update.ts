import IRoleAssignment from '../../interfaces/interface.RoleAssignment';
import RoleAssignmentModel from '../../models/model.roleAssignment';

export default async ( id: string, roleAssignment: IRoleAssignment ): Promise<IRoleAssignment|null> => {
  const updatedRoleAssignment = await RoleAssignmentModel.findOneAndUpdate({ _id:id },roleAssignment, { new:true });
  return updatedRoleAssignment? updatedRoleAssignment.toObject(): null;
};