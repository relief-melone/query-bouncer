import IRole from '../../interfaces/interface.Role';
import RoleModel from '../../models/model.role';

export default async (Title: string): Promise<IRole | Record<string,undefined> > =>{
  const role = await RoleModel.findOneAndDelete({ Title });
  return role ?  role.toObject() : {};
};