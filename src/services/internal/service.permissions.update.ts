import IPermission from '../../interfaces/interface.Permission';
import PermissionModel from '../../models/model.permission';

export default (permissionModel = PermissionModel) => async (Title: string,Permission: IPermission): Promise<IPermission | Record<string,undefined>> =>{
  const permission = await permissionModel.findOneAndUpdate({ Title },Permission,{ new:true });
  return permission? permission.toObject():{};
};