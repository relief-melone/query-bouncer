
import { RequestHandler } from 'express';
import ConfigMain from '../../configs/config.main';
import errorHandler from '../../controllers/errors/controller.errorHandler';
import isAdmin from '../auth/service.auth.admin';
import errorFactory from '../error/service.errors';

export const getService = (configMain = ConfigMain):RequestHandler =>
  async (req,res,next) => {
    const token = req.headers.authorization ? req.headers.authorization.replace('Bearer ', '') : null;
    if (!isAdmin(token, configMain)) {
      errorHandler(errorFactory.unauthorized('Invalid Authorization information!'), res);
    } else {
      next();
    }
  };


export default getService();