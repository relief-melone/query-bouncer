import { Right } from '../../interfaces/interface.Permission';

export default (right: string): Right | null => 
  right in Right
    ? Right[right] as Right
    : null;
