import specialUsers from '../../interfaces/enum.specialUsers';
import RoleAssignment from '../../interfaces/interface.RoleAssignment';
import RoleAssignmentModel from '../../models/model.roleAssignment';

export const getService = (roleAssignmentModel=RoleAssignmentModel) =>
  async (user: string | null, ): Promise<Array<RoleAssignment>> => {
    try {
      const users = user
        ? [ user, specialUsers.anyone,specialUsers.authenticated ]
        : [specialUsers.anyone];
      const roleAssignments =  await roleAssignmentModel.find( {
        User:{ $in:users }
      });
      return roleAssignments.map(ra => ra.toObject()) as RoleAssignment[];
    } catch (err) {
      throw err;
    }
  };


export default getService();