import PermissionModel from '../../models/model.permission';
import { getService as getPermissionBy } from '../internal/service.permissions.getBy';


export default getPermissionBy(PermissionModel);
