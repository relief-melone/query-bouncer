import ConfigMongodb from '@/classes/config.mongodb';

const config = new ConfigMongodb();

export default config;