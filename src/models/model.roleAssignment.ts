import RoleAssignment from '../interfaces/interface.RoleAssignment';
import RoleAssignmentSchema from '../schemas/schema.roleAssignment';
import mongoose from '../services/mongodb/service.mongodbConnector';

const RoleAssignmentModel = mongoose.model<RoleAssignment>('roleassignments', RoleAssignmentSchema);
RoleAssignmentModel.createIndexes();
RoleAssignmentModel.ensureIndexes();

export default RoleAssignmentModel;