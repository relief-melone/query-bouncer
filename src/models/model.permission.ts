import Permission from '../interfaces/interface.Permission';
import PermissionSchema from '../schemas/schema.permission';
import mongoose from '../services/mongodb/service.mongodbConnector';

const PermissionModel = mongoose.model<Permission>('permissions', PermissionSchema);

PermissionModel.createIndexes();

export default PermissionModel;