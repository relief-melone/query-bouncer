import Role from '../interfaces/interface.Role';
import RoleSchema from '../schemas/schema.role';
import mongoose from '../services/mongodb/service.mongodbConnector';

const RoleModel = mongoose.model<Role>('roles', RoleSchema);
RoleModel.createIndexes();

export default RoleModel;