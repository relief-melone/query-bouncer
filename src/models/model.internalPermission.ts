import Permission from '../interfaces/interface.Permission';
import PermissionSchema from '../schemas/schema.permission';
import mongoose from '../services/mongodb/service.mongodbConnector';

const InternalPermissionModel = mongoose.model<Permission>('internal_permissions', PermissionSchema);

InternalPermissionModel.createIndexes();

export default InternalPermissionModel;