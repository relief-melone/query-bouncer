import Config from './config';

export default class ConfigMongodb extends Config {

  constructor(env = process.env){
    super(env);
  }

  get connectionString():string{
    return this.env('MONGODB_CONNECTION_STRING');
  }
}