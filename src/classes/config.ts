import { getEnvVar, MandatoryEnvVarOpts } from '@/helpers/helper.getEnvVar';

export default class Config {
  _env: typeof process.env;

  constructor(env = process.env){
    this._env = env;
  }

  updateEnv(env = process.env){
    this._env = env;
  }

  env(envVarName: string, defaultVal?: string, opts:MandatoryEnvVarOpts = {}):string{
    return getEnvVar(this._env)(envVarName, defaultVal, opts);
  }
}

