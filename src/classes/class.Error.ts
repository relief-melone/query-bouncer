import log from '@/log/logger.default';

class Err {
  code: number;
  msg: string;
  details?: any;

  constructor(Code: number, Msg: string, Details?: any){
    this.code = Code;
    this.msg = Msg;
    if (Details){
      this.details = Details;
    }

    log.info(this);
  }
}

export { Err };
