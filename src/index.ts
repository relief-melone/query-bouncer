import expressConfig from '@/configs/config.express';
import configSessionPopulator from '@/configs/config.sessionPopulator';
import routeMain from '@/routes/route.main';
import { createInternalPermissionRouter, createPermissionRouter } from '@/routes/route.permissions';
import routeRoleAssignments from '@/routes/route.roleAssignments';
import routeRoles from '@/routes/route.roles';
import grantPreflight from '@/services/middleware/grantPreflight';
import setCorsHeaders from '@/services/middleware/setCorsHeaders';
import { connectToDb } from '@/services/mongodb/service.mongodbConnector';
import cookieParser from 'cookie-parser';
import express from 'express';
import sessionPopulate from 'rm-session-populator';
import log from './log/logger.default';

export default async () => {
  const app = express();
  app.use(setCorsHeaders);
  app.use(grantPreflight);
  app.use(express.json());
  app.use(express.urlencoded({
    extended: true
  }));

  app.use(cookieParser());
  app.use(sessionPopulate(configSessionPopulator));

  // Connect To Database
  await connectToDb();

  // Routes
  app.use('/api/admin/internalPermissions', createInternalPermissionRouter());
  app.use('/api/admin/permissions', createPermissionRouter());
  app.use('/api/admin/roles', routeRoles);
  app.use('/api/admin/roleAssignments', routeRoleAssignments);
  app.use('/api/v1', routeMain);
  
  // Liveness and root endpoint
  app.get('/liveness', (_, res) => {
    log.info('sending liveness probe');
    res.status(200).send('ok');
  });
  app.get('/', (_, res) => res.send('<h1>Query Bouncer is running!</h1>'));
 

  app.listen(expressConfig.httpPort, () => {
    console.log(`HTTP Server listening on ${expressConfig.httpPort}`);
  });


};

