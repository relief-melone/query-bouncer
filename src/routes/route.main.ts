import express from 'express';
import controllerMain from '../controllers/controller.main.ts';

const router = express.Router();

router.put('/:Collection/:Right', controllerMain);

export default router;