import express, { Router } from 'express';
import permissionsCreateController from '../controllers/controller.permissions.create';
import permissionsDeleteControler from '../controllers/controller.permissions.delete';
import permissionsGetController from '../controllers/controller.permissions.get';
import permissionsUpdateController from '../controllers/controller.permissions.update';
import getPopulatedPermissionsForCurrentUserController from '../controllers/controller.populatedPermissions.getForCurrentUser';
import tokenChecker from '../services/middleware/service.middleware.tokenauth';

import Permission from '@/interfaces/interface.Permission';
import { Model } from 'mongoose';
import InternalPermissionModel from '../models/model.internalPermission';
import PermissionModel from '../models/model.permission';
import CreatePermission from '../services/internal/service.permissions.create';
import DeletePermission from '../services/internal/service.permissions.delete';
import GetPermissions from '../services/internal/service.permissions.get';
import UpdatePermission from '../services/internal/service.permissions.update';
export { createInternalPermissionRouter, createPermissionRouter };

function createPermissionRouter(): Router{
  return createPermissions(PermissionModel);
}

function createInternalPermissionRouter(): Router{
  return createPermissions(InternalPermissionModel);
}

function createPermissions(model:Model<Permission>): Router{

  const router = express.Router();
  router.get('/myPermissions',  getPopulatedPermissionsForCurrentUserController(model));

  router.use('/', tokenChecker);
  router.post('/', permissionsCreateController(CreatePermission(model)));
  router.get('/', permissionsGetController(GetPermissions(model)));
  router.put('/:title', permissionsUpdateController(UpdatePermission(model)) );
  router.delete('/:title', permissionsDeleteControler(DeletePermission(model)) );
  return router;
}