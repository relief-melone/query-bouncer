import winston, { format, transports } from 'winston';
import configLogger from '../configs/config.logger';

export const getLogger = (config = configLogger) => 
  winston.createLogger({
    level: config.logLevel,
    format: format.json(),
    transports: [
      new transports.Console()
    ]
  });

export default getLogger();

